============
Key Features
============

Multi-User is a free and open source blender addon. 
It aims to allow multiple users to work on the same scene over the network. Based on a Clients / Server architecture, the data-oriented replication schema replicate blender data-blocks across the wire.

.. warning::
    The addon is still in development Be carefull when using it. 

    On rare occasions, it can happen that your blender scenes become corrupted, think of making backups to avoid losing your projects. 

Collaboration
=============

Multi-User allows a strong collaborative workflow between users. Being able to collaborate in this way has opened up new opportunities:


- Being able to create together and in real time on the same 3D scene, with instant feedback.
- Being able to teach directly in the same 3D environment in real time, facilitating communication between the teacher and these students.
- To be able to experiment with several people, to make challenges or simply to have fun.
- And much more !

Easier communication
====================

Thanks to *presence*, the overlay system that Multi-User provides, it is possible to see other users in the 3D space. The *presence* overlay is customizable to match your preferences (visibility, names, options).

Session management
==================

The addon works on a session system. The creator of the session and the administrators have rights that allow them to easily manage the session (backups, user management). In addition, there is a management of datablock rights so that each user can collaborate as they wish.